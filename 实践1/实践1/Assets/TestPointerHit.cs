﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TestPointerHit : VRTK_Pointer
{
    public override void PointerEnter(RaycastHit givenHit)
    {
        base.PointerEnter(givenHit);

        Debug.Log(givenHit.collider.gameObject);
    }
}
